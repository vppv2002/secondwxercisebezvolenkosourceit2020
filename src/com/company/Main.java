package com.company;

public class Main {

    public static void main(String[] args) {
        double a = 0;
        double b = 2;
        double c = 1;
        double discriminant;
        if (a != 0) {
            discriminant = b * b - 4 * a * c;
            if (discriminant == 0) {
                double result = -b / 2 * a;
                System.out.println("x = " + result);
            } else if (discriminant < 0)
                System.out.println("Так как дискриминант меньше нуля, то уравнение не имеет действительных решений.");
            else {
                double firstResult = (-b + Math.sqrt(discriminant)) / (2 * a);
                double secondResult = (-b - Math.sqrt(discriminant)) / (2 * a);
                System.out.println("Так как дискриминант больше нуля то, квадратное уравнение имеет два действительных корня:");
                System.out.println("Первый корень:" + firstResult);
                System.out.println("Второй корень:" + secondResult);
            }
        } else {
            System.out.println("Коэффициент при первом слагаемом уравнения не может быть равным нулю измените его и попробуйте снова.");
        }
    }
}

